package internal_test

import (
	"context"
	"github.com/jackc/pgx/v5"
	"gitlab.com/maen-bn/shortener/internal"
	"os"
	"testing"
)

func setupTestDb(ctx context.Context, tb testing.TB) (string, func(tb testing.TB)) {

	connStr, has := os.LookupEnv("TEST_DB_CONNECTION_STRING")
	if !has {
		tb.Fatalf("expected to have DB connection string")
	}
	conn, err := pgx.Connect(ctx, connStr)
	if err != nil {
		tb.Fatalf("could not connect to test DB, %s", err)
	}

	query, err := os.ReadFile("../scripts/create_original_urls.sql")
	if err != nil {
		tb.Fatalf("cannot read create original urls SQL file, %s", err)
	}

	_, err = conn.Exec(ctx, string(query))
	if err != nil {
		tb.Fatalf("cannot create original_urls table, %s", err)
	}

	_, err = conn.Exec(ctx, "INSERT INTO original_urls (url) VALUES ('https://slusny.net')")
	if err != nil {
		tb.Fatalf("cannot add test data to original_urls table, %s", err)
	}

	return connStr, func(tb testing.TB) {
		_, err = conn.Exec(ctx, "TRUNCATE original_urls RESTART IDENTITY")
		if err != nil {
			tb.Fatalf("could not wipe test data from original_urls table, %s", err)
		}
		_ = conn.Close(ctx)
	}
}

func TestNewPostgresWillEstablishAConnection(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping as it's not a short test")
	}
	ctx := context.Background()
	connStr, teardown := setupTestDb(ctx, t)
	defer teardown(t)

	p, err := internal.NewPostgres(connStr)
	if err != nil {
		t.Fatalf("did not expect error when initilising Postgres struct, %s", err)
	}

	if err = p.Conn.Ping(ctx); err != nil {
		t.Fatalf("expected to have an established connection when initilising Postgres struct, %s", err)
	}
}

func TestPostgresLoad(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping as it's not a short test")
	}
	ctx := context.Background()
	connStr, teardown := setupTestDb(ctx, t)
	defer teardown(t)

	p, err := internal.NewPostgres(connStr)
	if err != nil {
		t.Fatalf("did not expect error when initilising Postgres struct, %s", err)
	}

	tests := []struct {
		name        string
		id          int
		expectedUrl string
		expectError bool
	}{
		{
			name:        "load should give back a url string when passing a valid ID",
			id:          1,
			expectedUrl: "https://slusny.net",
			expectError: false,
		},
		{
			name:        "load should give back an error when passing a invalid ID",
			id:          9999,
			expectedUrl: "",
			expectError: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := p.Load(tt.id)
			if tt.expectError && err == nil {
				t.Fatalf("expected error got none")
			}
			if !tt.expectError && err != nil {
				t.Fatalf("expected no error got %s", err)
			}

			if tt.expectedUrl != got {
				t.Fatalf("expected %s, got %s", tt.expectedUrl, got)
			}
		})
	}

}

func TestSaveOrKey(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping as it's not a short test")
	}
	ctx := context.Background()
	connStr, teardown := setupTestDb(ctx, t)
	defer teardown(t)

	p, err := internal.NewPostgres(connStr)
	if err != nil {
		t.Fatalf("did not expect error when initilising Postgres struct, %s", err)
	}

	tests := []struct {
		name       string
		url        string
		expectedID int
	}{
		{
			name:       "save or key should give back an existing ID when passing an existing URL",
			url:        "https://slusny.net",
			expectedID: 1,
		},
		{
			name:       "save or key should give back a new ID when passing a new URL",
			url:        "https://www.yoyovillage.co.uk",
			expectedID: 2,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := p.SaveOrKey(ctx, tt.url)
			if err != nil {
				t.Fatalf("expected no error got %s", err)
			}

			if tt.expectedID != got {
				t.Fatalf("expected %d, got %d", tt.expectedID, got)
			}
		})
	}
}

func TestPing(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping as it's not a short test")
	}
	ctx := context.Background()
	connStr, teardown := setupTestDb(ctx, t)
	defer teardown(t)

	p, err := internal.NewPostgres(connStr)
	if err != nil {
		t.Fatalf("did not expect error when initilising Postgres struct, %s", err)
	}

	tests := []struct {
		name           string
		disconnect     bool
		expectedResult bool
		expectError    bool
	}{
		{
			name:           "ping with active connection should give a true result",
			disconnect:     false,
			expectedResult: true,
			expectError:    false,
		},
		{
			name:           "ping with inactive connection should give a false result",
			disconnect:     true,
			expectedResult: false,
			expectError:    true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.disconnect {
				p.Conn.Close()
			}

			got, err := p.Ping(ctx)
			if tt.expectError && err == nil {
				t.Fatalf("expected to have an error got %s", err)
			}
			if !tt.expectError && err != nil {
				t.Fatalf("expected to have no error got %s", err)
			}
			if tt.expectedResult != got {
				t.Fatalf("expected ping result to be %t, got %t", tt.expectedResult, got)
			}
		})
	}
}
