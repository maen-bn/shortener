package internal_test

import (
	"bytes"
	"context"
	"fmt"
	"gitlab.com/maen-bn/shortener"
	"gitlab.com/maen-bn/shortener/internal"
	"go.uber.org/zap/zaptest"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

type fakeStorage struct {
	urls   map[int]string
	nextId int
	dbOk   bool
}

func (s *fakeStorage) SaveOrKey(_ context.Context, url string) (int, error) {
	if !s.dbOk {
		return 0, fmt.Errorf("something wrong with the database")
	}
	for k, v := range s.urls {
		if v == url {
			return k, nil
		}
	}
	id := s.nextId
	s.urls[id] = url
	s.nextId++
	return id, nil
}

func (s *fakeStorage) Load(id int) (string, error) {
	if !s.dbOk {
		return "", fmt.Errorf("something wrong with the database")
	}
	return s.urls[id], nil
}

func (s *fakeStorage) Ping(_ context.Context) (bool, error) {
	if !s.dbOk {
		return false, fmt.Errorf("something wrong with the database")
	}
	return true, nil
}

func TestNewApiSpecificationGivesNoErrorWhenMandatoryEnvVarsArePresent(t *testing.T) {
	_ = os.Setenv("SHORTENER_REDIRECTOR_URI_SCHEME", "https")
	_ = os.Setenv("SHORTENER_REDIRECTOR_URI_HOST", "shortener.com")
	_ = os.Setenv("SHORTENER_DB_URL", "postgresql://user:password@fake:5432/shortener")
	defer func() {
		_ = os.Unsetenv("SHORTENER_REDIRECTOR_URI_SCHEME")
		_ = os.Unsetenv("SHORTENER_REDIRECTOR_URI_HOST")
		_ = os.Unsetenv("SHORTENER_DB_URL")
	}()

	_, err := internal.NewApiSpecification()
	if err != nil {
		t.Fatalf("did not expect error, %s", err)
	}
}

func TestNewApiSpecificationReturnsErrorWhenMandatoryEnvVarsAreNotPresent(t *testing.T) {
	_, err := internal.NewApiSpecification()
	if err == nil {
		t.Fatal("expected error and got none")
	}
}

func TestShortenerHandler(t *testing.T) {
	tests := []struct {
		name                 string
		method               string
		requestBody          []byte
		requestParam         string
		dbOk                 bool
		existingUrls         map[int]string
		expectedStatus       int
		expectedResponseBody string
	}{
		{
			name:                 "can shorten",
			method:               http.MethodPost,
			requestBody:          []byte(`{"url":"https://www.test.com"}`),
			dbOk:                 true,
			expectedStatus:       http.StatusOK,
			expectedResponseBody: `{"link":"https://myshortener.com/b"}`,
		},
		{
			name:                 "can't shorten if there is an internal error",
			method:               http.MethodPost,
			requestBody:          []byte(`{"url":"https://www.test.com"}`),
			dbOk:                 false,
			expectedStatus:       http.StatusInternalServerError,
			expectedResponseBody: `{"msg":"problem shortening URL, something wrong with the database"}`,
		},
		{
			name:                 "method not allowed when using PATCH",
			method:               http.MethodPatch,
			requestBody:          []byte(`{"url":"https://www.test.com"}`),
			dbOk:                 true,
			expectedStatus:       http.StatusMethodNotAllowed,
			expectedResponseBody: `{"msg":"Method not allowed"}`,
		},
		{
			name:                 "bad request when json not provided",
			method:               http.MethodPost,
			requestBody:          []byte(``),
			dbOk:                 true,
			expectedStatus:       http.StatusBadRequest,
			expectedResponseBody: `{"msg":"problem decoding or parsing your provided URL, EOF"}`,
		},
		{
			name:                 "can retrieve original",
			method:               http.MethodGet,
			requestParam:         "url=https://myshortener.com/b",
			dbOk:                 true,
			existingUrls:         map[int]string{1: "https://www.test.com"},
			expectedStatus:       http.StatusOK,
			expectedResponseBody: `{"link":"https://www.test.com"}`,
		},

		{
			name:                 "can't retrieve original if there is an internal error",
			method:               http.MethodGet,
			requestParam:         "url=https://myshortener.com/b",
			dbOk:                 false,
			existingUrls:         map[int]string{1: "https://www.test.com"},
			expectedStatus:       http.StatusInternalServerError,
			expectedResponseBody: `{"msg":"problem retrieving original URL: something wrong with the database"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := newServer(t, tt.dbOk, tt.existingUrls)

			url := fmt.Sprintf("/shorten?%s", tt.requestParam)
			req, err := http.NewRequest(tt.method, url, bytes.NewBuffer(tt.requestBody))
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(srv.ShortenHandler)
			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != tt.expectedStatus {
				t.Errorf("expected status code not returned: got %v want %v", status, tt.expectedStatus)
			}

			if rr.Body.String() != tt.expectedResponseBody {
				t.Errorf(
					"expected response body not returned: got %v want %v", rr.Body.String(), tt.expectedResponseBody,
				)
			}
		})
	}
}

func TestHealthCheckHandler(t *testing.T) {
	tests := []struct {
		name                 string
		dbOk                 bool
		existingUrls         map[int]string
		httpMethod           string
		expectHttpCode       int
		expectedResponseBody string
	}{
		{
			name:                 "expect healthy database connection to return a HTTP 200",
			dbOk:                 true,
			httpMethod:           http.MethodGet,
			expectHttpCode:       http.StatusOK,
			expectedResponseBody: `{"database":{"ok":true,"msg":""},"healthy":true}`,
		},
		{
			name:                 "expect un-healthy database connection to return a HTTP 500",
			dbOk:                 false,
			httpMethod:           http.MethodGet,
			expectHttpCode:       http.StatusInternalServerError,
			expectedResponseBody: `{"database":{"ok":false,"msg":"something wrong with the database"},"healthy":false}`,
		},
		{
			name:                 "expect an error trying to use an invalid HTTP method",
			dbOk:                 true,
			httpMethod:           http.MethodPost,
			expectHttpCode:       http.StatusMethodNotAllowed,
			expectedResponseBody: `{"msg":"Method not allowed"}`,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			srv := newServer(t, tt.dbOk, tt.existingUrls)
			req, err := http.NewRequest(tt.httpMethod, "/health", bytes.NewBuffer([]byte{}))
			if err != nil {
				t.Fatal(err)
			}

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(srv.HealthCheckHandler)
			handler.ServeHTTP(rr, req)

			if tt.expectHttpCode != rr.Code {
				t.Fatalf("expect HTTP response code %d, got %d", tt.expectHttpCode, rr.Code)
			}

			if tt.expectedResponseBody != rr.Body.String() {
				t.Fatalf("expected response body %s, got %s", tt.expectedResponseBody, rr.Body.String())
			}
		})
	}
}

func TestDocsHandler(t *testing.T) {
	srv := newServer(t, true, make(map[int]string))

	req, err := http.NewRequest(http.MethodGet, "/docs/", bytes.NewBuffer([]byte{}))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.DocsHandler)
	handler.ServeHTTP(rr, req)

	if http.StatusOK != rr.Code {
		t.Fatalf("expected HTTP code %d, got %d", http.StatusOK, rr.Code)
	}

	expected := "text/html"
	got := rr.Header().Get("Content-Type")
	if expected != got {
		t.Fatalf("expected content to be %s, got %s", expected, got)
	}
}

func TestServerStartAndShutdownDoesNotReturnErrorsDuringNormalOperation(t *testing.T) {
	srv := newServer(t, true, make(map[int]string))
	errs := make(chan error, 1)
	go func() {
		err := srv.Start(":8080")
		if err != nil {
			errs <- err
		}
	}()

	// Adding a bit of a wait buffer to give sufficient time
	// for the server to start
	time.Sleep(1 * time.Second)

	err := srv.Shutdown(1 * time.Microsecond)
	if err != nil {
		t.Fatalf("Expected no server shutdown errors, got %s", err)
	}

	close(errs)
	if len(errs) > 0 {
		t.Fatalf("Expected no server start errors, got %s", <-errs)
	}
}

func newServer(t *testing.T, dbOk bool, existingUrls map[int]string) *internal.Server {
	if existingUrls == nil {
		existingUrls = make(map[int]string)
	}
	store := &fakeStorage{
		urls:   existingUrls,
		nextId: 1,
		dbOk:   dbOk,
	}

	cfg := shortener.NewConfig("https", "myshortener.com", "abcdefghijklmnopqrstuvwxyz0123456789", store)
	logger := zaptest.NewLogger(t)
	return &internal.Server{
		Config: cfg,
		Logger: logger.Sugar(),
	}
}
