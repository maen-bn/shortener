package internal

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

// Postgres embeds the postgres connection and allows the storage.Storage to be satisfied
type Postgres struct {
	Conn *pgxpool.Pool
}

// NewPostgres connects to a postgres instance using the provided connection string and then constructs a fresh Postgres
// The underlying Postgres.Conn field is a pool so can be reused and is concurrent safe.
func NewPostgres(connString string) (*Postgres, error) {
	conn, err := pgxpool.New(context.Background(), connString)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("failed to connect to database: %v", err))
	}

	return &Postgres{conn}, nil
}

// SaveOrKey will look to see if the url is already been shortened, otherwise it'll create a new record in the DB
func (p *Postgres) SaveOrKey(ctx context.Context, url string) (int, error) {
	var id int

	tx, err := p.Conn.Begin(ctx)
	if err != nil {
		return 0, err
	}
	defer tx.Rollback(ctx)

	err = tx.QueryRow(context.Background(), "SELECT id FROM original_urls WHERE url=$1", url).Scan(&id)
	if err != nil && err != pgx.ErrNoRows {
		return 0, err
	}
	if id > 0 {
		return id, nil
	}

	s := "INSERT INTO original_urls (url) VALUES ($1) returning id;"
	err = tx.QueryRow(context.Background(), s, url).Scan(&id)
	if err != nil {
		return 0, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return 0, err
	}

	return id, nil
}

// Load will give back the original url that was used to be shortened
func (p *Postgres) Load(id int) (string, error) {
	var url string
	err := p.Conn.QueryRow(context.Background(), "SELECT url FROM original_urls WHERE id=$1", id).Scan(&url)
	if err != nil {
		return "", err
	}

	return url, nil
}

// Ping is just using pgx's ping method off of pgxpool.Pool but with the addition of a boolean returned as well
// as an error message
func (p *Postgres) Ping(ctx context.Context) (bool, error) {
	err := p.Conn.Ping(ctx)
	if err != nil {
		return false, err
	}

	return true, nil
}
