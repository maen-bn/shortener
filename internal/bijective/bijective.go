// Package bijective implements a basic way to get obtain a slice of integers from a seed
// that can be inverted back to the original seed
package bijective

// Encode takes the provided seed and base to produce an invertible slice of integers
func Encode(seed, base int) (numbers []int) {
	if seed == 0 {
		numbers = append(numbers, 0)
	}

	for i := seed; i > 0; i = i / base {
		remainder := i % base
		numbers = append(numbers, remainder)
	}
	return reverse(numbers)
}

// Decode inverts the slice of integers provided in Encode and returns the original seed
func Decode(numbers []int, base int) int {
	seed := 0
	for _, i := range numbers {
		seed = seed*base + i
	}

	return seed
}

func reverse(numbers []int) []int {
	for i := len(numbers)/2 - 1; i >= 0; i-- {
		opp := len(numbers) - 1 - i
		numbers[i], numbers[opp] = numbers[opp], numbers[i]
	}

	return numbers
}
