package bijective_test

import (
	"gitlab.com/maen-bn/shortener/internal/bijective"
	"testing"
)

func TestEncodedCanBeDecodedBackToOriginalSeed(t *testing.T) {
	seed := 1986
	encoded := bijective.Encode(seed, 62)
	decoded := bijective.Decode(encoded, 62)
	if seed != decoded {
		t.Fatalf("expected seed to be %d got %d", seed, decoded)
	}
}

func TestZeroSeedReturnsArrayWithOneElementThatIsZero(t *testing.T) {
	expected := []int{0}
	actual := bijective.Encode(0, 62)
	if !equal(expected, actual) {
		t.Fatalf("expected %v got %v", expected, actual)
	}
}

func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
