package internal

import (
	"context"
	_ "embed"
	"encoding/json"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gitlab.com/maen-bn/shortener"
	"go.uber.org/zap"
	"net/http"
	"net/url"
	"time"
)

// ApiSpecification defines the env vars required to run the API.
type ApiSpecification struct {
	// RedirectorUriScheme refers to where you intend to host your publicly available redirects when
	// a user enters the shorten URL into their browser.
	// This API does not handle the redirects as it's conforming to the architectural
	// constraints of REST while allowing flexibility for any client that chooses to
	// use this API
	RedirectorUriScheme string `required:"true" split_words:"true"`

	// RedirectorUriHost refers to where you intend to host your publicly available redirects when
	// a user enters the shorten URL into their browser.
	// This API does not handle the redirects as it's conforming to the architectural
	// constraints of REST while allowing flexibility for any client that chooses to
	// use this API
	RedirectorUriHost string `required:"true" split_words:"true"`

	DbUrl            string `required:"true" split_words:"true"`
	ListenAddress    string `default:":8080" split_words:"true"`
	AlphaNumericBase string `default:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" split_words:"true"`
}

// NewApiSpecification provides a convenient constructor for mapping the env vars needed for the API to ApiSpecification.
func NewApiSpecification() (*ApiSpecification, error) {
	var s ApiSpecification
	err := envconfig.Process("shortener", &s)

	if err != nil {
		return nil, err
	}

	return &s, nil
}

type urlBody struct {
	URL string `json:"url"`
}

// Server acts as our HTTP server along the dependencies required to operate the server.
// Via Server, we can register the routing for all our HTTP handlers.
type Server struct {
	httpServer *http.Server
	Config     *shortener.Config
	Logger     *zap.SugaredLogger
}

//go:embed docs.html
var docs []byte

// Start registers the routes and the listening address and then starts up the http server.
// It's useful to call this within a go routine, so you can listen out for any os signals
// and gracefully shut down on any signal that causes the program to stop abruptly via
// Shutdown
func (s *Server) Start(listenAddress string) error {
	mux := http.NewServeMux()
	mux.HandleFunc("/shorten", s.ShortenHandler)
	mux.HandleFunc("/health", s.HealthCheckHandler)
	mux.HandleFunc("/docs/", s.DocsHandler)

	s.httpServer = &http.Server{Addr: listenAddress, Handler: mux}

	if err := s.httpServer.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}

	return nil
}

// Shutdown can be utilised to gracefully shut down the http server in Server as well
// as giving the option to provide a timeout, so we aren't waiting too long on any
// long-running operations
func (s *Server) Shutdown(timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	if err := s.httpServer.Shutdown(ctx); err != nil {
		return err
	}

	return nil
}

// ShortenHandler takes the URL provided by the client, passing it to the shortener, and returns the result as JSON.
// For POST methods it'll try to shorten the URL and return it, and for GET methods it'll retrieve the non-shorten
// URL based on the provided shorten URL in the request param "?url="
func (s *Server) ShortenHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method != http.MethodPost && r.Method != http.MethodGet {
		s.writeMethodNotAllow(w)
		return
	}

	u, err := s.decodeAndParse(r)
	if err != nil {
		s.writeErrorResponse(w, http.StatusBadRequest, fmt.Errorf("problem decoding or parsing your provided URL, %s", err))
		return
	}

	var link *url.URL

	switch r.Method {
	case http.MethodPost:
		link, err = s.Config.Shorten(r.Context(), u)
		if err != nil {
			s.writeErrorResponse(w, http.StatusInternalServerError, fmt.Errorf("problem shortening URL, %s", err))
			return
		}
	case http.MethodGet:
		link, err = s.Config.Original(u)
		if err != nil {
			s.writeErrorResponse(w, http.StatusInternalServerError, fmt.Errorf("problem retrieving original URL: %v", err))
			return
		}
	}

	_, err = w.Write([]byte(fmt.Sprintf(`{"link":"%s"}`, link)))
	if err != nil {
		s.Logger.Infof("Could not write response: %v", err)
	}
}

// HealthCheckHandler basically just does a simple check to establish if the database connection is available
func (s *Server) HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "no-cache")

	if r.Method != http.MethodGet {
		s.writeMethodNotAllow(w)
		return
	}

	type responseBody struct {
		Database struct {
			Ok  bool   `json:"ok"`
			Msg string `json:"msg"`
		} `json:"database"`
		Healthy bool `json:"healthy"`
	}

	var rspBdy responseBody
	rspBdy.Healthy = true
	httpCode := http.StatusOK

	dbAlive, err := s.Config.Storage.Ping(r.Context())
	if err != nil {
		rspBdy.Healthy = false
		rspBdy.Database.Msg = err.Error()
		httpCode = http.StatusInternalServerError
	}
	rspBdy.Database.Ok = dbAlive

	b, err := json.Marshal(rspBdy)
	if err != nil {
		s.Logger.Infof("Could not write response: %v", err)
	}

	w.WriteHeader(httpCode)
	_, err = w.Write(b)
	if err != nil {
		s.Logger.Infof("Could not write response: %v", err)
	}

}

func (s *Server) DocsHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	if _, err := w.Write(docs); err != nil {
		s.Logger.Infof("Could not write response: %v", err)
	}
}

func (s *Server) writeMethodNotAllow(w http.ResponseWriter) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	_, err := w.Write([]byte(`{"msg":"Method not allowed"}`))
	if err != nil {
		s.Logger.Infof("Could not write %d msg: %v", http.StatusMethodNotAllowed, err)
	}
}

func (s *Server) decodeAndParse(r *http.Request) (*url.URL, error) {
	// We use a request param for parsing the URL when the request is a GET because
	// the HTTP spec says a request body has defined semantics meaning for a GET
	// so problems can arise e.g. if using a proxy/load balancer or trying to
	// cache the request
	if r.Method == http.MethodGet {
		u := r.URL.Query().Get("url")
		return url.Parse(u)
	}

	var body urlBody
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, err
	}

	return url.Parse(body.URL)
}

func (s *Server) writeErrorResponse(w http.ResponseWriter, httpCode int, err error) {
	w.WriteHeader(httpCode)
	s.Logger.Infof("%s", err)
	_, err = w.Write([]byte(fmt.Sprintf(`{"msg":"%s"}`, err)))
	if err != nil {
		s.Logger.Infof("Could not write %d msg: %v", httpCode, err)
	}
}
