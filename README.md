# Shortener

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/maen-bn/shortener.svg)](https://pkg.go.dev/gitlab.com/maen-bn/shortener)
[![pipeline status](https://gitlab.com/maen-bn/shortener/badges/main/pipeline.svg)](https://gitlab.com/maen-bn/shortener/-/commits/main)
[![coverage report](https://gitlab.com/maen-bn/shortener/badges/main/coverage.svg)](https://gitlab.com/maen-bn/shortener/-/commits/main)

A basic URL shortener implemented in Go utilising a simple bijective to encode a seed (such as a unique storage ID) to an alphanumeric string. 

## Usage options

Shortener can be used in the following ways:

* As a module dependency
* As a Web API

## Module dependency usage

```sh
go get -t gitlab.com/maen-bn/shortener
```
Example code. Please note, use as a module dependency requires you provide your own storage logic currently by implementing `shortener.Storage`.

```go
package main

import (
	"context"
	"log"
	"net/url"

	"gitlab.com/maen-bn/shortener"
)

func main() {
	// Implement shortener.Storage for your choice of storage
	store := &MyStorage{}
	base := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	cfg := shortener.NewConfig("http", "localhost", base, store)
	u := &url.URL{Scheme: "https", Host: "test.com", Path: "somepath?q=searchingsomething"}
	shortened, err := cfg.Shorten(context.Background(), u)
	if err != nil {
		log.Fatalf("could not shorten URL, %s\n", err)
	}
	log.Printf("shorten URL: %s\n", shortened)

	orig, err := cfg.Original(shortened)
	if err != nil {
		log.Fatalf("could not get back original from shorten URL %s, %s\n", shortened, err)
	}

	log.Printf("original URL: %s\n", orig)
}
```

## Web API usage

### Build/install

You can either build the web API yourself:
```shell
git clone https://gitlab.com/maen-bn/shortener.git
cd shortener
CGO_ENABLED=0 go build -trimpath -o  bin/api ./cmd/api/
```
and the binary will be in `bin/`

Or use a container image provided in the registry for this repo

```shell
docker login registry.gitlab.com
## Pull the latest
docker pull registry.gitlab.com/maen-bn/shortener/api
## Or pull a specific version
docker pull registry.gitlab.com/maen-bn/shortener/api:v0.0.3
```

### Dependencies

The web API expects the following dependencies:

* [PostgreSQL](https://www.postgresql.org) (tested against 15.3)
	* You need to run `scripts/create_original_urls.sql` to set up the required DB table
* [Go 1.20.x](https://go.dev) if you're building the web API yourself
* [Docker](https://www.docker.com) (if you're using the image from the container registry)
* A web server where you intend to carry out the redirection
	* See the FAQ below for why the web API is not responsible for the redirections and that you need to provide somewhere to do the redirection
* The following environment variables
	* `SHORTENER_REDIRECTOR_URI_SCHEME` -  (STRING **required**) The URI scheme your redirection server uses e.g. https
	* `SHORTENER_REDIRECTOR_URI_HOST` - (STRING **required**) The host of your redirection server
	* `SHORTENER_DB_URL` - (STRING **required**) The URL connection string
	* `SHORTENER_LISTEN_ADDRESS` - (STRING **optional**) The listen address you want this web API to run on. Defaults to `:8080`
	* `SHORTENER_ALPHA_NUMERIC_BASE` - (STRING **optional**) The base you want to use to generate the path of the shortened URLs. Defaults to `abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789`

#### Quick start examples

If you built the web API yourself, make sure you ran `scripts/create_original_urls.sql` to set up the required DB table in your PostgreSQL instance follow the example below changing the var values to reflect your setup:

Example
```shell
SHORTENER_REDIRECTOR_URI_SCHEME=https SHORTENER_REDIRECTOR_URI_HOST=test.com SHORTENER_DB_URL=postgres://user:password@localhost:5432/shortener bin/api
```

If you're using a provided container image from the registry you can 

Example
```shell
docker network create shortener-network
curl -o create_original_urls.sql https://gitlab.com/maen-bn/shortener/-/raw/main/scripts/create_original_urls.sql
docker run --name shortener-postgres -e POSTGRES_PASSWORD=password --network shortener-network -v /${PWD}/create_original_urls.sql:/docker-entrypoint-initdb.d/create_original_urls.sql:ro -d postgres:15.3-alpine
docker run --name shortener-api -e SHORTENER_REDIRECTOR_URI_SCHEME=https -e SHORTENER_REDIRECTOR_URI_HOST=myshortener-redirector.com -e SHORTENER_DB_URL=postgres://postgres:password@shortener-postgres:5432/postgres -p 8080:8080 --network shortener-network -d registry.gitlab.com/maen-bn/shortener/api
```

Or for an easier quick start using the image from the registry you can use the `docker-compose-example.yml` from this repository

Example
```shell
curl -o create_original_urls.sql https://gitlab.com/maen-bn/shortener/-/raw/main/docker-compose-example.yml
docker compose -f docker-compose-example.yml up
```

### Documentation

There is an [OpenAPI 3 spec](https://gitlab.com/maen-bn/shortener/-/blob/main/internal/openapi.yaml) which can also be accessed from the API from the endpoint `/docs/`.

## FAQ

### Why does the web API not do any redirects? / Why do I have to provide a separate server for doing the redirects when running the web API?

The web API tries to follow REST architectural constraints as much as possible. In REST, you're trying to represent resources and using HTTP statuses like `301` when calling `GET` `/shorten` gives the impression the resource has moved which it hasn't. 
This becomes further problem-matic when trying to follow the REST constraint Hypermedia as the Engine of Application State (HATEOAS) yet that hypermedia information you're communicating is not getting to the client if you're redirecting them.

Also, not redirecting clients while giving them back the original URL via `GET` `/shorten` gives flexibility of clients for ease of use e.g. machine to machine.

### Why are you not using a third party HTTP mux?

The HTTP handlers in the web API are so basic it seems quite excessive to pull in a third party HTTP mux with all the baggage that comes with keeping up to date with those type of external packages, [or worst yet them going out of support](https://github.com/gorilla/mux).
