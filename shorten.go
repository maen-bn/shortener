// Package shortener provides a simple way to configure URL shortening as well as shorten a URL and retrieve the
// original.
package shortener

import (
	"context"
	"fmt"
	"gitlab.com/maen-bn/shortener/internal/bijective"
	"net/url"
	"strings"
)

// Config provides a setup to define your specific shortening hosting as well as the alphanumric base and storage
// to use for generating and storing the shortened URLs.
type Config struct {
	Scheme           string
	Host             string
	AlphaNumericBase string
	Storage          Storage
}

// NewConfig is a convenient constructor for setting up a Config.
func NewConfig(scheme string, host string, alphaNumericBase string, store Storage) *Config {
	return &Config{
		Scheme:           scheme,
		Host:             host,
		AlphaNumericBase: alphaNumericBase,
		Storage:          store,
	}
}

// Shorten saves the url that needs shortening to the provided storage and uses the return id as the seed to
// do a bijective encode to a path alias for the shortened URL.
func (c *Config) Shorten(ctx context.Context, toShort *url.URL) (*url.URL, error) {
	id, err := c.Storage.SaveOrKey(ctx, toShort.String())
	if err != nil {
		return nil, err
	}
	encoded := bijective.Encode(id, len(c.AlphaNumericBase))

	var path string
	for _, num := range encoded {
		path = path + string(c.AlphaNumericBase[num])
	}

	return &url.URL{
		Scheme: c.Scheme,
		Host:   c.Host,
		Path:   "/" + path,
	}, nil
}

// Original remaps the shortened path to the set of numbers that can be decoded back to it's seed. Once the decoded
// is done, the seed is used to return the original as the ID stored in the storage was used in Shorten as the seed.
func (c *Config) Original(shortened *url.URL) (*url.URL, error) {
	if err := c.validateShortened(shortened); err != nil {
		return nil, err
	}

	var numbers []int
	path := shortened.Path[1:]
	for i := range path {
		numbers = append(numbers, strings.Index(c.AlphaNumericBase, string(path[i])))
	}

	seed := bijective.Decode(numbers, len(c.AlphaNumericBase))
	urlStr, err := c.Storage.Load(seed)
	if err != nil {
		return nil, err
	}

	return url.Parse(urlStr)
}

func (c *Config) validateShortened(shortened *url.URL) error {
	if shortened.Scheme != c.Scheme {
		return fmt.Errorf(
			"shortened URL scheme \"%s\" does not match the config's \"%s\"",
			shortened.Scheme,
			c.Scheme,
		)
	}
	if shortened.Host != c.Host {
		return fmt.Errorf(
			"shortened URL host \"%s\" does not match the config's \"%s\"",
			shortened.Host,
			c.Host,
		)
	}
	if len(shortened.Path) < 2 {
		return fmt.Errorf("url path is too short")
	}

	return nil
}
