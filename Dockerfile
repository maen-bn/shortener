FROM golang:1.20-alpine as build
WORKDIR /go/src/app
COPY . .
RUN GOOS=linux CGO_ENABLED=0 go build -trimpath -ldflags="-s -w" -o  bin/api ./cmd/api/

FROM scratch
COPY --from=build /go/src/app/bin/api .

CMD ["/api"]