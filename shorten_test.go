package shortener_test

import (
	"context"
	"fmt"
	"gitlab.com/maen-bn/shortener"
	"net/url"
	"testing"
)

type fakeStorage struct {
	urls   map[int]string
	nextId int
	dbOk   bool
}

func (s *fakeStorage) SaveOrKey(_ context.Context, url string) (int, error) {
	if !s.dbOk {
		return 0, fmt.Errorf("something wrong with the database")
	}
	for k, v := range s.urls {
		if v == url {
			return k, nil
		}
	}
	id := s.nextId
	s.urls[id] = url
	s.nextId++
	return id, nil
}

func (s *fakeStorage) Load(id int) (string, error) {
	if !s.dbOk {
		return "", fmt.Errorf("something wrong with the database")
	}
	return s.urls[id], nil
}

func (s *fakeStorage) Ping(_ context.Context) (bool, error) {
	return true, nil
}

func TestShorten(t *testing.T) {
	ctx := context.Background()
	store := &fakeStorage{
		urls:   make(map[int]string),
		nextId: 1,
		dbOk:   true,
	}
	cfg := shortener.NewConfig("http", "localhost", "abcdefghijklmnopqrstuvwxyz0123456789", store)

	tests := []struct {
		name      string
		seed      int
		toShorten *url.URL
		want      string
	}{
		{
			name:      "first storage id",
			seed:      0,
			toShorten: &url.URL{Scheme: "https", Host: "test.com", Path: "/somepath?q=searchingsomething"},
			want:      "/b",
		},
		{
			name:      "larger storage id",
			seed:      54354352345435,
			toShorten: &url.URL{Scheme: "https", Host: "test.com", Path: "/list/all"},
			want:      "/tjwbv98kt",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.seed != 0 {
				store.nextId = tt.seed
			}
			got, err := cfg.Shorten(ctx, tt.toShorten)
			if err != nil {
				t.Fatalf("did not expect error: %v", err)
			}
			if tt.want != got.Path {
				t.Fatalf("%s: expected: %v, got: %v", tt.name, tt.want, got.Path)
			}
		})
	}
}

func TestShortenReturnsErrorWhenThereIsAStorageIssue(t *testing.T) {
	ctx := context.Background()
	store := &fakeStorage{
		urls:   make(map[int]string),
		nextId: 1,
		dbOk:   false,
	}
	cfg := shortener.NewConfig("http", "localhost", "abcdefghijklmnopqrstuvwxyz0123456789", store)
	_, err := cfg.Shorten(ctx, &url.URL{Scheme: "https", Host: "test.com", Path: "/list/all"})
	if err == nil {
		t.Fatal("expected error, got nil")
	}
}

func TestShortenCanBeInVersedBackToTheOriginal(t *testing.T) {
	store := &fakeStorage{
		urls:   make(map[int]string),
		nextId: 1,
		dbOk:   true,
	}
	cfg := shortener.NewConfig("http", "localhost", "abcdefghijklmnopqrstuvwxyz0123456789", store)

	want := &url.URL{Scheme: "https", Host: "test.com", Path: "/list/all"}
	short, err := cfg.Shorten(context.Background(), want)
	if err != nil {
		t.Fatalf("did not expect error: %v", err)
	}

	got, err := cfg.Original(short)
	if err != nil {
		t.Fatalf("did not expect error: %v", err)
	}

	if want.String() != got.String() {
		t.Fatalf("expected: %v, got: %v", want, got)
	}
}

func TestOriginalErrors(t *testing.T) {
	tests := []struct {
		name      string
		shortened *url.URL
		dbOk      bool
	}{
		{
			name:      "expect error when URL path is too short",
			shortened: &url.URL{Scheme: "https", Host: "localhost", Path: ""},
			dbOk:      true,
		},
		{
			name:      "expect error when there is a storage issue",
			shortened: &url.URL{Scheme: "https", Host: "localhost", Path: "/dsfd8d7as"},
			dbOk:      false,
		},
		{
			name:      "expect error when the shortened URL scheme does not match the config",
			shortened: &url.URL{Scheme: "http", Host: "localhost", Path: "/dsfd8d7as"},
			dbOk:      true,
		},
		{
			name:      "expect error when the shortened URL host does not match the config",
			shortened: &url.URL{Scheme: "https", Host: "wronghost", Path: "/dsfd8d7as"},
			dbOk:      true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			store := &fakeStorage{
				urls:   make(map[int]string),
				nextId: 1,
				dbOk:   tt.dbOk,
			}
			cfg := shortener.NewConfig("https", "localhost", "abcdefghijklmnopqrstuvwxyz0123456789", store)
			_, err := cfg.Original(tt.shortened)

			if err == nil {
				t.Fatal("expected err but got nil")
			}
		})
	}
}
