package main

import (
	"gitlab.com/maen-bn/shortener"
	"gitlab.com/maen-bn/shortener/internal"
	"go.uber.org/zap"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatal(err.Error())
	}
	spec, err := internal.NewApiSpecification()
	if err != nil {
		logger.Sugar().Fatalf("Failed to create specification: %v", err)
	}

	store, err := internal.NewPostgres(spec.DbUrl)

	if err != nil {
		logger.Sugar().Fatalf("Failed to connect to store: %v", err)
	}

	cfg := shortener.NewConfig(spec.RedirectorUriScheme, spec.RedirectorUriHost, spec.AlphaNumericBase, store)
	srv := internal.Server{Config: cfg, Logger: logger.Sugar()}

	srv.Logger.Infof("Starting server on address %s,", spec.ListenAddress)
	go func() {
		err = srv.Start(spec.ListenAddress)
		if err != nil {
			srv.Logger.Fatal(err)
		}
	}()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	<-sigs

	err = srv.Shutdown(30 * time.Second)
	if err != nil {
		srv.Logger.Fatalf("Graceful shutdown failed, %s", err)
	}

	srv.Logger.Infof("Successfully shutdown")
}
