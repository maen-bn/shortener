ENV_SET = GOOS=linux CGO_ENABLED=0

test:
	$(ENV_SET) go test -short -v ./...

test-all:
	$(ENV_SET) go test -v ./...

coverage:
	$(ENV_SET)
	go test -v ./... -coverprofile profile.cov
	go tool cover -func profile.cov

checkfmt:
	$(ENV_SET) test -z $(gofmt -l .)
vet:
	go vet ./...