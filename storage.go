package shortener

import "context"

// Storage is implemented by any type that wants to store and retrieve original URLs for shortening.
type Storage interface {
	SaveOrKey(ctx context.Context, url string) (int, error)
	Load(id int) (string, error)
	Ping(ctx context.Context) (bool, error)
}
